import React from "react";
import { Router } from "@reach/router";
import { StoreProvider } from "./Store";
import ReactDOM from "react-dom";

import App from "./App";
import HomePage from "./Features/HomePage";
import { GamesPage } from "./Features/GamesPage";

ReactDOM.render(
  <StoreProvider>
    <Router>
      <App path="/">
        <HomePage path="/" />
        <GamesPage path="/games" />
      </App>
    </Router>
  </StoreProvider>,
  document.getElementById("root")
);
