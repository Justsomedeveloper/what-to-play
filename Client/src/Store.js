import React from "react";

export const Store = React.createContext();
const initialState = {
  games: [],
  backlogs: []
};

const reducer = (state, action) => {
  switch (action.type) {
    case "FETCH_GAMES":
      return { ...state, games: action.payload };
    case "FETCH_BACKLOGS":
      return { ...state, backlogs: action.payload };
    default:
      return state;
  }
};

export const StoreProvider = props => {
  const [state, dispatch] = React.useReducer(reducer, initialState);
  const value = { state, dispatch };
  return <Store.Provider value={value}>{props.children}</Store.Provider>;
};
