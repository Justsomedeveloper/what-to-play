import axios from "axios";
import { config } from "./config";

export const igdbAPI = axios.create({
  baseURL: "https://cors-anywhere.herokuapp.com/https://api-v3.igdb.com/",
  headers: {
    Accept: "application/json",
    "user-key": config.IGDB_KEY
  }
});

export const whatToPlayApi = axios.create({
  baseURL: "http://localhost:3000"
});
