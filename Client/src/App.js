import React from "react";

export default function App(props) {
  return (
    <React.Fragment>
      <header className="header">
        <div>
          <h1>What to play </h1>
        </div>
      </header>
      {props.children}
    </React.Fragment>
  );
}
