import React from "react";
import { Store } from "../../Store";

export const GameList = () => {
  const { state } = React.useContext(Store);

  const games = state.games;

  return games.map(game => {
    return (
      <div key={game.id}>
        <img src={game.cover.url} />
        <h1>{game.name}</h1>
      </div>
    );
  });
};
