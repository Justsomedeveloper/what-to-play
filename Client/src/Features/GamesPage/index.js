import React, { useState, useEffect } from "react";
import { Store } from "../../Store";
import { GameList } from "../GameList";
import { igdbAPI } from "../../api";

export const GamesPage = () => {
  const { dispatch } = React.useContext(Store);
  const [query, setQuery] = useState("");
  const [search, setSearch] = useState("");

  useEffect(() => {
    const fetchGames = async () => {
      const games = await igdbAPI.post(
        "games",
        `fields cover.url,id, name, platforms.name, release_dates.date, screenshots.url,summary;search "${search}"; where cover.url != null;`
      );

      return dispatch({
        type: "FETCH_GAMES",
        payload: games.data
      });
    };
    fetchGames();
  }, [search, dispatch]);

  return (
    <>
      <h1>Search games here!</h1>
      <input
        value={query}
        onChange={event => setQuery(event.target.value)}
        onKeyPress={event => {
          if (event.key === "Enter") {
            setSearch(query);
          }
        }}
      />
      <GameList />
    </>
  );
};
