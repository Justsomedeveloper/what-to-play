import React, { useState, useEffect } from "react";
import { Store } from "../../Store";
import { whatToPlayApi } from "../../api";

export default function HomePage() {
  const { state, dispatch } = React.useContext(Store);
  const [backlogName, setBacklogName] = useState("");
  const backlogs = state.backlogs;

  const createBacklog = async () => {
    const games = await whatToPlayApi.post("/backlogs/create", {
      name: backlogName,
      userId: "5c44924d02729741703fc3cc"
    });
  };

  useEffect(() => {
    const fetchBacklogs = async () => {
      const backlogs = await whatToPlayApi.get(
        "/backlogs?userId=5c44924d02729741703fc3cc"
      );

      return dispatch({
        type: "FETCH_BACKLOGS",
        payload: backlogs.data
      });
    };
    fetchBacklogs();
  }, [dispatch]);

  return (
    <React.Fragment>
      <section>
        <h1>Welcome to the What-To-Play application! </h1>
      </section>
      <div>
        <h2>Backlogs:</h2>
        {backlogs.map(backlog => {
          return <h3>{backlog.name} </h3>;
        })}
      </div>
      <p>Create a backlog here </p>
      <input
        value={backlogName}
        onChange={event => setBacklogName(event.target.value)}
      />
      <button onClick={() => createBacklog()}> Create</button>
    </React.Fragment>
  );
}
