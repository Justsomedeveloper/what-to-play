const express = require("express");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
const cors = require("cors");
const app = express();

const backlog = require("./routes/backlog.route");
const user = require("./routes/user.route");
const config = require("./config");

app.use(cors());

/* MONGODB CONFIG */
let dev_db_url = config.dev_db_url;
let mongoDB = process.env.MONGODB_URI || dev_db_url;
mongoose.connect(mongoDB);
mongoose.Promise = global.Promise;
let db = mongoose.connection;
db.on("error", console.error.bind(console, "MongoDB connection error:"));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use("/backlogs", backlog);
app.use("/users", user);

const port = 3000;

app.listen(port, () => console.log(`listening on port ${port}`));
