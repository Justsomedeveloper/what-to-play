const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let UserSchema = new Schema({
    username: {type: String, require: true, max: 30},
    // Eventually encrypt this, for inital release plain text is okay.
    password: {type: String, require: true, max: 15}
})

module.exports = mongoose.model('User', UserSchema);