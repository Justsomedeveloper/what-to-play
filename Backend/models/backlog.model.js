const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let BacklogSchema = new Schema({
    name: { type: String, required: true, max: 50 },
    user: { type: Schema.Types.ObjectId, ref: 'User',  required: true},
    games: [{
        apiId: { type: Number, required: true },
        name: { type: String, required: true },
        description: { type: String },
        platforms: [
            {
                name: {type: String}
            }
        ],
        genres: [
            {
                name: {type: String}
            }
        ]
    }]
})

module.exports = mongoose.model('Backlog', BacklogSchema);