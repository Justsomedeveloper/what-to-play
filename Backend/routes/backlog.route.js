const express = require('express');
const router = express.Router();

const backlog_controller = require('../controllers/backlog.controller');

router.get('/:id', backlog_controller.backlog_details);

router.get('/', backlog_controller.backlogs_get);

router.post('/create', backlog_controller.backlog_create);

router.put('/:id/update', backlog_controller.backlog_update);

router.delete('/:id/delete', backlog_controller.backlog_delete);

module.exports = router;