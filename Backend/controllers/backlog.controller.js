const Backlog = require("../models/backlog.model");
const User = require("../models/user.model");

/* Create a backlog */
exports.backlog_create = (req, res) => {
  let backlog = new Backlog({
    name: req.body.name,
    user: req.body.userId
  });

  backlog.save(err => {
    if (err) {
      return next(err);
    }
    res.send("success");
  });
};

/* Get all backlogs by user ID */
exports.backlogs_get = (req, res) => {
  Backlog.find({ user: req.query.userId }, (err, backlogs) => {
    if (err) return next(err);
    res.send(backlogs);
  });
};

/* Get specific backlog */
exports.backlog_details = (req, res) => {
  Backlog.findById(req.params.id, (err, backlog) => {
    if (err) {
      return err;
    }
    res.send(backlog);
  });
};

exports.backlog_update = (req, res) => {
  Backlog.findByIdAndUpdate(
    req.params.id,
    {
      $set: {
        name: req.body.name
      },
      $push: {
        games: req.body.game
      }
    },
    (err, backlog) => {
      if (err) {
        console.log(err);
        return err;
      }
      res.send("Backlog Updated");
    }
  );
};

exports.backlog_delete = (req, res) => {
  Backlog.findByIdAndDelete(req.params.id, err => {
    if (err) return next(err);
    res.send("Backlog Deleted");
  });
};
