const User = require('../models/user.model');

/* Create a user */
exports.user_create = (req, res) => {
    let user = new User({
        username: req.body.username,
        password: req.body.password
    });

    user.save((err) => {
        if(err) {
            return next(err);
        }
        res.send('User created successfully');
    })
}

exports.user_details = (req, res) => {
    User.findById(req.params.id, (err, user) => {
        if(err) {
            return err;
        }
        res.send(user)
    })
}